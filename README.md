					      Tailscale-Nextcloud
						Secure & Private 





**#Update**

`sudo apt update && sudo apt upgrade`

**#Basics installieren**

`sudo apt install curl wget unzip`

**#Samba installieren:**

`sudo apt install samba smbclient cifs-utils` 

`sudo nano /etc/samba/smb.conf`

<details><summary>Click to expand</summary>
workgroup = WORKGROUP
</details>
<details><summary>Click to expand</summary>
server role = member server
</details>


`sudo systemctl restart smbd`

**#Mariadb**

`sudo apt -y install mariadb-server mariadb-client`

`sudo mysql_secure_installation `

> 
> 
> 
> NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB

>       SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!
> In order to log into MariaDB to secure it, we'll need the current

> password for the root user.  If you've just installed MariaDB, and

> you haven't set the root password yet, the password will be blank,

> so you should just press enter here.

Enter current password for root (enter for none): 

OK, successfully used password, moving on...

> Setting the root password ensures that nobody can log into the MariaDB

> root user without the proper authorisation.

> Set root password? [Y/n] y

> New password: 

> Re-enter new password: 

> Password updated successfully!

> Reloading privilege tables..

>  ... Success!

> By default, a MariaDB installation has an anonymous user, allowing anyone

> to log into MariaDB without having to have a user account created for

> them.  This is intended only for testing, and to make the installation

> go a bit smoother.  You should remove them before moving into a

> production environment.
|
> Remove anonymous users? [Y/n] y

>  ... Success!

> |Normally, root should only be allowed to connect from 'localhost'.  This

> ensures that someone cannot guess at the root password from the network.

> Disallow root login remotely? [Y/n] y
> ... Success!

> By default, MariaDB comes with a database named 'test' that anyone can
> access.  This is also intended only for testing, and should be removed

> before moving into a production environment.

> Remove test database and access to it? [Y/n] y
>  - Dropping test database...
> ... Success!
>  - Removing privileges on test database...
>  ... Success!
> Reloading the privilege tables will ensure that all changes made so far
> will take effect immediately.
> |Reload privilege tables now? [Y/n] y
> Cleaning up...
> All done!  If you've completed all of the above steps, your MariaDB
> installation should now be secure.
> |Thanks for using MariaDB!



**#Mysql**

`sudo mysql -u root -p` 

`CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY 'StarkesPasSworD';`

`CREATE DATABASE nextcloud;`

`GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'localhost';`

`FLUSH PRIVILEGES`;

`QUIT`

**#lösche alte PHP Bestände**

`sudo apt -y remove php php-{cli,xml,zip,curl,gd,cgi,mysql,mbstring}`

**# Install PHP 8.3**
`sudo apt update`

`sudo apt install lsb-release apt-transport-https ca-certificates software-properties-common`

`sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg`

`sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'`

`sudo apt update`

`sudo apt install php8.3`

`sudo apt install nano php8.3-{bcmath,xml,fpm,mysql,zip,intl,ldap,gd,cli,bz2,curl,mbstring,pgsql,opcache,soap,cgi,mysql,imagick,ctype,dom,fileinfo,GD,posix,SimpleXML,XMLReader,XMLWriter}`

`sudo apt install apache2 libapache2-mod-php8.3`

`sudo nano /etc/php/*/apache2/php.ini`

`date.timezone = Europa/Berlin`

`memory_limit = 512M #2 GB unter der max RAM Belegung`

`upload_max_filesize = 40G`

`post_max_size = 41G`

`max_execution_time = 3600`

`;output_buffering            # lösche das Semikolon und kommentiere aus (#)`


**#Apache2 neu starten**

`sudo systemctl restart apache2`

**#Download Nextcloud**

`wget https://download.nextcloud.com/server/releases/latest.zip`

`unzip latest.zip`

`rm -f latest.zip`

`sudo mv nextcloud /var/www/html/`

`sudo chown -R www-data:www-data /var/www/html/nextcloud`

`sudo sudo chmod -R 755 /var/www/html/nextcloud`

**#Apache Server Konfiguration**

`sudo a2dissite 000-default.conf`

`sudo rm /var/www/html/index.html`

`sudo systemctl restart apache2`

**#Starte den Nextcloud Web installer**

`http://{server-ip|hostname]/nextcloud`
 
**#Apache2 installieren**

`sudo apt install apache2` 

**#Tailscale installieren und einrichten**

`curl -fsSL https://tailscale.com/install.sh | sh`

`sudo tailscale up`

`sudo tailscale login`

`sudo tailscale cert`

**#Das Routing bitte an das eigene Netz anpassen**

`echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.d/99-tailscale.conf`

`echo 'net.ipv6.conf.all.forwarding = 1' | sudo tee -a /etc/sysctl.d/99-tailscale.conf`

`sudo sysctl -p /etc/sysctl.d/99-tailscale.conf`

`sudo tailscale up --advertise-routes=192.168.1.0/24,10.0.5.0/24`

`sudo nano /var/www/html/nextcloud/config/config.php`

>   'trusted_domains' => 
>   array (
>     0 => '192.168.1.XXX',
>     1 => 'server.taildeb.net',
>     3 => 'tailscale-ipv6 ',
>     2 => 'tailscale-ipv4',
>     4 => 'localhost',


**#ufw**

`sudo apt install ufw`

**#ufw ssh**

`sudo ufw allow from 192.168.1.0/24 to any port ssh`

**#ufw Samba**

`sudo ufw allow from 192.168.1.0/24 to any app Samba`

**#Tailscale**

`sudo ufw allow 3478/udp`

`sudo ufw allow 41641/udp`

**#https**

`sudo ufw allow https`

`sudo ufw allow 443`


`sudo ufw status`

> Status: active
> 
> To                         Action      From
> --                         ------      ----
>                   
> 22/tcp                     ALLOW       192.168.1.0/24            
> 41641/udp                  ALLOW       Anywhere                  
> 3478/udp                   ALLOW       Anywhere                  
> Samba                      ALLOW       192.168.1.0/24            
> 443/tcp (v6)               ALLOW       Anywhere (v6)             
> 41641/udp (v6)             ALLOW       Anywhere (v6)             
> 3478/udp (v6)              ALLOW       Anywhere (v6) 
   

`sudo ufw enable && sudo ufw reload`

`sudo systemctl start ufw`

`sudo systemctl enable ufw`

**#fail2ban**

`sudo apt install fail2ban`

`sudo touch /var/log/auth.log`

`systemctl status fail2ban`

`sudo systemctl start fail2ban`

`sudo systemctl enable fail2ban`

`sudo cp /etc/fail2ban/jail.local /etc/fail2ban/jail.local.bak`

`sudo nano /etc/fail2ban/jail.local`

```
[Ban Time Multipliers]
 bantime.increment = true
 bantime.factor = 2
 bantime.formula = ban.Time * (1<<(ban.Count if ban.Count<20 else 20)) * banFactor

 [DEFAULT]
 ignoreip = 127.0.0.1/8 ::1 192.168.1.0/24
 banaction = ufw
 findtime = 60h
 maxretry = 2
 destemail = felix.scheibelt@ik.me
 sender = fail2ban@example.com


 [apache-badbots]
 enabled = true
 port = http,https
 logpath = %(apache_access_log)s
 bantime = 48h
 maxretry = 1

 [apache-botsearch]
 enabled = true
 port = http,https
 logpath = %(apache_error_log)s
 banaction = cloudflare
 bantime = 72h
 maxretry = 1

 [sshd]
 enabled = true
 port = 22 #ssh
 filter = sshd
 logpath = /var/log/auth.log
 maxretry = 3
 findtime = 300
 bantime = 3600
```



`sudo systemctl restart fail2ban`

`sudo fail2ban-client reload`

`sudo systemctl status fail2ban`

**#Antivirus**

`sudo apt install clamav-freshclam` 

`sudo freshclam`


**#Das gesammte System scannen und infizierte Datein entfernen**

`clamscan -r --remove /` 

**#Backup**

`sudo apt install timeshift`

`sudo timeshift --create --comments "Nextcloud-1" --tags w`


**#Custom Style**

APP custom css installieren


**Unter Design->custom css**
________________________

```
.board {
  padding-left: 14px !important;
}

.stack {
  background: var(--color-background-hover) !important;
  margin-bottom: 16px;
}

.stack__header::before {
  background-image: none !important;
}

.stack__header {
  background-color: var(--color-primary-light);
  align-items: center;
}

.stack__title {
  font-weight: bold;
}

.smooth-dnd-draggable-wrapper .stack:first-of-type {
  margin-left: 0;
}

.smooth-dnd-container.vertical {
  padding-top: 22px !important;
}
```



**#Optionales https mit selfsigned Cert**

`sudo tailscale cert "eure-tailscale-domain.net"`

`sudo nano /etc/apache2/sites-available/000-default.conf`

`<VirtualHost *:80>
    ServerAdmin eure@email
    DocumentRoot /var/www/html/nextcloud
    ServerName "eure-tailscale-domain.net"
    Redirect / https://"eure-tailscale-domain.net"/
</VirtualHost>`

`sudo a2enmod ssl`

`sudo a2enmod headers`

`sudo a2ensite default-ssl`

`sudo a2enconf ssl-params`

`sudo apache2ctl configtest`

`sudo systemctl restart apache2`

`sudo systemctl reload apache2`

**#Datenbank einlesen**

`cd /var/www/html/nextcloud/ && sudo -u www-data php occ files:scan --all`

**#Nextcloud Update**

`mysqldump -u root -p nextcloud > ~/nextcloud.sql`


`cd /var/www/html/nextcloud/`

`sudo tar -cpzvf ~/nextcloud-config.tar.gz config/`

`sudo -u www-data php8.3 updater/updater.phar --no-interaction`

`sudo -u www-data php8.3 /var/www/nextcloud/occ upgrade`

`sudo -u www-data php8.3 /var/www/html/nextcloud/occ upgrade`

`sudo -u www-data php8.3 /var/www/html/nextcloud/occ db:add-missing-indices`

`sudo -u www-data php8.3 /var/www/html/nextcloud/occ db:convert-filecache-bigint`

`sudo -u www-data crontab -e`

**#Am Ende der "Liste eintragen um Nachts jede Nacht um 3 Uhr ein Update an zu stossen**

`0 3 * * * php8.3 /var/www/nextcloud/updater/updater.phar --no-interaction`


__________________________







Quellen:

[nextcloud](https://computingforgeeks.com/how-to-install-and-configure-nextcloud-on-debian/)

[Nextcloud II](https://gitlab.com/tiagotome95/NextcloudServerConfig/-/tree/master/selfsigned)

[Tailscale Exit Node](https://tailscale.com/kb/1103/exit-nodes?q=exit+node)

[Tailscale subnets](https://tailscale.com/kb/1019/subnets?q=subnet)

[fail2ban](https://www.linuxcapable.com/how-to-install-fail2ban-on-debian-linux/)

[clamav](https://www.howtoforge.com/tutorial/clamav-ubuntu/)

[Timeshift](https://dev.to/rahedmir/how-to-use-timeshift-from-command-line-in-linux-1l9b)

[Custom CSS](https://github.com/nextcloud/deck/issues/1892)

[https Redirect](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-16-04)

[selfsigned Tailscale](https://community.letsencrypt.org/t/tailscale-certificates/198895/11)

[Nextcloud Update](https://www.linuxbabe.com/cloud-storage/upgrade-nextcloud-command-line-gui)

License The documentation in this project is licensed under the Creative Commons Attribution-ShareAlike 4.0 license, the source code content (also the source code included in the documentation) is licensed under the GNU GPLv3 license.
Author of Aknowledgement T.Scheibelt felix.scheibelt@ik.me  
 
[[Session-ID][(https://getsession.org/)](https://getsession.org/):0547ee5afe021e22f03ced91ec52671eee6a6e2e1115f1fe30332b5486f4db8c6f






